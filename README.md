# biFrost

**A Bridge between EC2 and SSH**

**biFrost** will use a defined AWS profile to get a list of ASG's and the instances they contain, and then allow you to connect to them.

## Dependancies

`tabulate` - used to display data in a tabular format
```
$ sudo pip install tabulate
```

## Usage

#### Configuration

###### To list all available profiles

```bash
$ ./config.py list
```

###### To add a new profile

```bash
$ ./config.py
```

This will ask you to enter a display name for the profile, the profile name (as setup when using 'aws configure') and the location of your private key.

You will then be able to use the profile display name when launching the biFrost application.


#### Application

```bash
$ ./bifrost.py [profile display name]
```

`[profile display name]` is a display name that was used when configuring the application