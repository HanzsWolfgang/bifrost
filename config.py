#!/usr/bin/env python

import argparse
import sqlite3
import os.path

from tabulate import tabulate


class Config(object):
    database_name = 'bifrost.db'
    table_name = 'config'

    def run(self):
        parser = argparse.ArgumentParser(description='Configure biFrost.')
        parser.add_argument("list", nargs='?', help="List existing profiles")
        args = parser.parse_args()

        if args.list is not None:
            try:
                self.list()
                exit()
            except sqlite3.OperationalError:
                print('No items to list')
        else:
            if os.path.isfile(self.database_name):
                self.configure()
            else:
                self.create_database()
                self.configure()

    def create_database(self):
        print "Creating Database..."
        print ""
        conn = sqlite3.connect(self.database_name)
        c = conn.cursor()
        c.execute('CREATE TABLE ' + self.table_name + ' ('
                  'display_name VARCHAR(200) DEFAULT NULL,'
                  'profile_name VARCHAR(200) DEFAULT NULL,'
                  'profile_key VARCHAR(200) DEFAULT NULL'
                  ')')
        conn.commit()
        conn.close()

    def configure(self):
        name = raw_input('Name: ')
        profile_name = raw_input('Profile Name: ')
        profile_key = raw_input('Profile Key: ')

        conn = sqlite3.connect(self.database_name)
        c = conn.cursor()
        c.execute('INSERT INTO ' + self.table_name + ' '
                  '(display_name, profile_name, profile_key) VALUES '
                  '("' + name + '", "' + profile_name + '", "' + profile_key + '")')

        conn.commit()
        conn.close()

    def list(self):
        conn = sqlite3.connect(self.database_name)
        c = conn.cursor()
        c.execute('SELECT * FROM ' + self.table_name)
        rows = c.fetchall()
        print ""
        print tabulate(rows, headers=['Display Name', 'Profile Name', 'Profile Key'])
        print ""
        conn.close()


config = Config()
config.run()
