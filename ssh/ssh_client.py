#!/usr/bin/env python

import getpass
import os
import socket
import sys
import traceback
from paramiko.py3compat import input

import paramiko

import interactive


class SSHClient(object):

    t = None
    private_key_file = None

    def agent_auth(self, username):
        try:
            key = paramiko.RSAKey.from_private_key_file(self.private_key_file)
            self.t.auth_publickey(username, key)
            return
        except paramiko.SSHException:
            sys.exit(1)

    def connect(self, username, hostname, private_key, port=22):

        self.private_key_file = private_key

        # setup logging
        paramiko.util.log_to_file('ssh.log')

        # now connect
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect((hostname, port))
        except Exception as e:
            traceback.print_exc()
            sys.exit(1)

        try:
            self.t = paramiko.Transport(sock)
            try:
                self.t.start_client()
            except paramiko.SSHException:
                print('*** SSH negotiation failed.')
                sys.exit(1)

            try:
                keys = paramiko.util.load_host_keys(os.path.expanduser('~/.ssh/known_hosts'))
            except IOError:
                try:
                    keys = paramiko.util.load_host_keys(os.path.expanduser('~/ssh/known_hosts'))
                except IOError:
                    print('*** Unable to open host keys file')
                    keys = {}

            # check server's host key -- this is important.
            key = self.t.get_remote_server_key()
            if hostname not in keys:
                print('')
            elif key.get_name() not in keys[hostname]:
                print('')
            elif keys[hostname][key.get_name()] != key:
                print('*** WARNING: Host key has changed!!!')
                sys.exit(1)

            # get username
            if username == '':
                default_username = getpass.getuser()
                username = input('Username [%s]: ' % default_username)
                if len(username) == 0:
                    username = default_username

            self.agent_auth(username)
            if not self.t.is_authenticated():
                print('*** Authentication failed. :(')
                self.t.close()

                sys.exit(1)

            chan = self.t.open_session()
            chan.get_pty(term='xterm')
            chan.invoke_shell()
            interactive.interactive_shell(chan)
            chan.close()
            self.t.close()
            sys.exit(0)

        except Exception as e:
            print('*** Caught exception: ' + str(e.__class__) + ': ' + str(e))
            traceback.print_exc()
            try:
                self.t.close()
            except:
                pass
            sys.exit(1)
