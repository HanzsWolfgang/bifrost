#!/usr/bin/env python

import argparse
import os.path
import sys
import sqlite3

import boto3

from application import Application

database_name = 'bifrost.db'
table_name = 'config'

profile_name = None
private_key = None

aws_root_path = os.path.abspath(os.path.join(os.getcwd(), os.pardir, os.pardir, os.pardir))
sys.path.append(aws_root_path)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='A bridge between ssh and ec2.')
    parser.add_argument("profile", help="the AWS profiles display name as defined in the config")
    args = parser.parse_args()

    profile = args.profile

    # Get profile details from database and pass to correct places
    conn = sqlite3.connect(database_name)
    c = conn.cursor()
    c.execute('SELECT profile_name, profile_key FROM ' + table_name + ' WHERE display_name = "' + profile + '" LIMIT 1')
    row = c.fetchone()

    if row is None:
        print("No record found for profile '" + profile + '"')
        exit()
    else:
        profile_name = row[0]
        private_key = row[1]

    boto3.setup_default_session(profile_name=profile_name, region_name="eu-west-1")

    try:
        app = Application()
        app.run(private_key)
    except KeyboardInterrupt as e:
        exit()
    except Exception as e:
        print e
