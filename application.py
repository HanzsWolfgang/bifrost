#!/usr/bin/python

import subprocess

import boto3
import botocore

import urwid

from ssh.ssh_client import SSHClient


class Application(object):
    ec2_client = None
    asg_client = None

    private_key = None

    screen = None

    current = None

    def run(self, private_key):

        self.private_key = private_key

        self.ec2_client = boto3.resource('ec2')
        self.asg_client = boto3.client('autoscaling')

        try:
            self.get_asgs()
        except botocore.exceptions.ClientError as e:
            print "======== Client Error ========"
            print e

        except Exception as e:
            print "======== General Error ========"
            print e

    def get_asgs(self):

        groups = {}
        for asg in self.asg_client.describe_auto_scaling_groups()['AutoScalingGroups']:
            groups[asg['AutoScalingGroupName']] = asg['AutoScalingGroupName']
        groups = sorted(groups)

        if len(groups) > 0:
            self.draw_menu(' Welcome to biFrost ', groups, "asg_menu")
        else:
            self.draw_menu(' Welcome to biFrost ', self.get_instances(""), "instance_menu")

    def get_instances(self, asg_name):
        if asg_name == "":
            return self.ec2_client.instances.filter(
                Filters=[
                    {'Name': 'instance-state-name', 'Values': ['running']}
                ]
            )
        else:
            return self.ec2_client.instances.filter(
                Filters=[
                    {'Name': 'instance-state-name', 'Values': ['running']},
                    {'Name': 'tag:aws:autoscaling:groupName', 'Values': [asg_name]}
                ]
            )

    def draw_menu(self, title, items, menu_method):

        menu = getattr(Application, menu_method)(self, title, items)
        main = urwid.Padding(menu, left=2, right=2)
        top = urwid.Overlay(main, urwid.SolidFill(u'\N{MEDIUM SHADE}'),
                            align='center', width=('relative', 60),
                            valign='middle', height=('relative', 70),
                            min_width=60, min_height=9)
        urwid.MainLoop(top, palette=[('reversed', 'standout', '')],
                       unhandled_input=self.on_key_press, handle_mouse=False).run()

    def asg_menu(self, title, items):
        self.current = "asg"
        body = [urwid.Text(title), urwid.Divider()]
        for item in items:
            button = urwid.Button(item)
            urwid.connect_signal(button, 'click', self.asg_chosen, item)
            body.append(urwid.AttrMap(button, None, focus_map='reversed'))
        return urwid.ListBox(urwid.SimpleFocusListWalker(body))

    def asg_chosen(self, button, choice):

        instances = self.get_instances(choice)
        self.draw_menu(choice, instances, "instance_menu")

    def instance_menu(self, title, items):
        self.current = "instance"
        body = [urwid.Text(title), urwid.Divider()]
        for item in items:
            item_name = item.id
            for tag in item.tags:
                if tag['Key'] == 'Name':
                    item_name += ' (' + tag['Value'] + ')'

            button = urwid.Button(item_name)
            urwid.connect_signal(button, 'click', self.instance_chosen, item.public_dns_name)
            body.append(urwid.AttrMap(button, None, focus_map='reversed'))
        return urwid.ListBox(urwid.SimpleFocusListWalker(body))

    def on_key_press(self, key):
        if key == 'esc':
            self.run(self.private_key)
        if key == 'backspace':
            if self.current == "instance":
                self.get_asgs()

    def instance_chosen(self, button, choice):
        subprocess.call('clear')

        hostname = choice
        username = "ubuntu"

        ssh = SSHClient()
        ssh.connect(username, hostname, self.private_key)
